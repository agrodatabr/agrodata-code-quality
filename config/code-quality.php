<?php

return [
    'phpcs' => [
        'ruleset' => base_path('vendor/agrodata/code-quality/src/rulesets/phpcs-ruleset.xml'),
        'paths' => [
            'app'
        ]
    ],
    'phpmd' => [
        'ruleset' => base_path('vendor/agrodata/code-quality/src/rulesets/phpmd-ruleset.xml'),
        'paths' => [
            'app'
        ],
    ]
];
