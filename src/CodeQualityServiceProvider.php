<?php

namespace Agrodata\CodeQuality;

use Illuminate\Support\ServiceProvider;

class CodeQualityServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/code-quality.php' => base_path('config/code-quality.php'),
        ], 'config');

        $this->mergeConfigFrom(__DIR__ . '/../config/code-quality.php', 'code-quality');
    }

    public function register()
    {
        $this->commands([
            \Agrodata\CodeQuality\Console\CodeQualityCommand::class
        ]);
    }
}
