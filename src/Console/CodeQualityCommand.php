<?php

namespace Agrodata\CodeQuality\Console;

use Illuminate\Console\Command;

class CodeQualityCommand extends Command
{
    protected $signature = 'code:quality {--phpcs} {--phpmd}';
    protected $description = 'Verify code quality with phpcs and phpmd tools';

    public function handle()
    {
        $phpcs = $this->option('phpcs');
        $phpmd = $this->option('phpmd');

        $this->checkDependencies();
        #$this->extractFilesToBeAnalysed(); #only modified files in git

        if (((!$phpcs && !$phpmd) || ($phpcs && !$phpmd))) {
            $this->runCodeSniffer()
                ? $this->output->success('Code Sniffer OK!')
                : $this->output->error('Something is wrong. Fix the Code Sniffer Errors.');
        }

        if (((!$phpcs && !$phpmd) || (!$phpcs && $phpmd))) {
            $this->runPHPMD()
                ? $this->output->success('Mess detector OK!')
                : $this->output->error('Something is wrong. Fix the Mess Detector Errors.');
        }
        exit(1);
    }

    private function checkDependencies()
    {
        $phpcs = $this->option('phpcs');
        $phpmd = $this->option('phpmd');

        $dependencies = [];

        if (((!$phpcs && !$phpmd) || ($phpcs && !$phpmd))) {
            $dependencies[] = "vendor".DIRECTORY_SEPARATOR."bin".DIRECTORY_SEPARATOR."phpcs";
        }
        if (((!$phpcs && !$phpmd) || (!$phpcs && $phpmd))) {
            $dependencies[] = "vendor".DIRECTORY_SEPARATOR."bin".DIRECTORY_SEPARATOR."phpmd";
        }

        $status = true;
        foreach ($dependencies as $dependency) {
            if (!file_exists(base_path($dependency))) {
                $this->output->error("The package [$dependency] wasn\'t found.");
                $status = false;
            }
        }
        if (!$status) {
            exit(1);
        }
    }

    private function runCodeSniffer(): bool
    {
        $this->output->info('Running Code Sniffer..');
        $slash = DIRECTORY_SEPARATOR;
        $outputs = [];

        foreach (config('code-quality.phpcs.paths') as $path) {
            $this->output->text("Scanning $path");
            $output = $this->executeCommand(
                ".{$slash}vendor{$slash}bin{$slash}phpcs --colors --standard={{ruleset}} {{path}}",
                $path,
                config('code-quality.phpcs.ruleset')
            );
            if (!empty($output)) {
                $this->output->writeln($output);
                $outputs[] = $output;
            }
        }

        return count($outputs) === 0;
    }

    private function runPHPMD(): bool
    {
        $this->output->info('Running Mess Detector..');
        $slash = DIRECTORY_SEPARATOR;
        $paths = config('code-quality.phpmd.paths');
        $outputs = [];
        foreach ($paths as $path) {
            $this->output->text("Scanning $path");
            $output = $this->executeCommand(
                ".{$slash}vendor{$slash}bin{$slash}phpmd {{path}} ansi {{ruleset}}",
                $path,
                config('code-quality.phpmd.ruleset')
            );
            if (count($output) > 4) {
                $this->output->writeln($output);
            }
            $outputs[] = $output;
        }

        return (count($outputs, COUNT_RECURSIVE) - count($paths)) === (4 * count($paths));
    }

    private function executeCommand(string $command, string $path, string $ruleset = ""): array
    {
        $command = str_replace("{{path}}", $path, $command);
        $command = str_replace("{{ruleset}}", $ruleset, $command);

        exec($command, $output);

        return $output;
    }

    /**
     * Run Code Beautifier and Fixer.
     *
     * private function runPHPCBF()
     * {
     *
     * }*/

    /**
     * Extract PHP files to be analysed from HEAD.
     *
    private function extractFilesToBeAnalysed()
    {
    exec("git diff --name-only --diff-filter=ACMR HEAD | grep .php", $this->files);
    }*/
}


